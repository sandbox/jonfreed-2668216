<?php
/**
 * @file
 * For view_area_button the install message and view area component list text.
 */

/**
 * Implements hook_install().
 *
 * Upon installation, display a message about this module's settings.
 */
function view_area_button_install() {
  $t = get_t();
  drupal_set_message($t('The "View area Button" module has been installed. '
    . 'There are no general administrative settings for the module.  Use the '
    . 'module to add a button to a view area header or footer to see '
    . 'use-specific settings. '
  ));
}
/**
 * Implements hook_views_data().
 *
 * Provide text for list of components for a view area, e.g. header or footer.
 */
function view_area_button_views_data() {
  $data['views']['view_area_button_area5'] = array(
    'title' => t('Button'),
    'help' => t('A configurable button for view areas like the header or footer. Could be used to provide an Add button. Has a configurable label, destination URI, and permission-based visibility.  An Add button can use Prepopulate module via the URI.'),
    'area' => array(
      'id' => 'view_area_button_area',
    ),
  );
  return $data;
}
