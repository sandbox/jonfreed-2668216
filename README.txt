README.txt for Drupal project module "View area Button"


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQs
 * Maintainers


INTRODUCTION
------------

Do you need a button in a view header or footer?  For example, an "Add" button 
for adding another item to the view?  if yes, then this project module is for 
you.

This project module enables you to display a configurable button in a view 
area like the header or footer. The button has a configurable label, 
destination URI, and permission-based visibility.  You could set up an "Add" 
button to use Prepopulate module via the configurable URI.

This project module enables a configurable button for view areas like the 
header or footer. It could be used to provide an Add button. It has a 
configurable label, destination URI, and permission-based visibility. An 
Add button can use Prepopulate module via the URI.


REQUIREMENTS
------------

Drupal 8.x.


RECOMMENDED MODULES
-------------------

 * Prepopulate
   You can set the URI for any View area Button you add to a view area (like a
   a header or footer.)  When you set up the URI you can use tokens in 
   combination with the Prepopulate module to set up an "Add" button for 
   adding a new node of the type that is in the view.

   
INSTALLATION
------------

Install like any other module.

For example, for Drupal 8, first get the path to the project module's gz or zip
file.  In your site, go to your Admin "Extend" page, click the button "Install
new module", and enter the path or select a local copy of the gz or zip file.
Then click the button "Install".  Then go to your site's admin list of 
available modules and enable the module.


CONFIGURATION
-------------

To use this module, edit a view and then in either in its section for header, 
footer, or empty, click the "Add" button and then in the pop-up select the 
"Button" item.

Then, follow the onscreen instructions for setting:
  1.  The permission a user must have to be able to see the button
  2.  The button's label
  3.  The button URI 

   
TROUBLESHOOTING
---------------

If you have an concern, create a new issue on Drupal.org in the project 
module's issue list.  The project module will be updated with troubleshooting 
steps if any common troubles are identified via that list.


FAQ
---

 1.  Q:  Why isn't the required permission box a drop-down select list?
     A:  Only because the developer has lacked time to figure that out.  Feel
         free to add to the project module's existing issue of type "Feature
         request" that is for that functionality.  Especially please add 
         references or code for how this can be done in Drupal 8.
         
 2.  Q:  How do I get a button to work with the checkboxes for selecting 
         particular nodes in a view? 
     A:  There is an existing project module issue of type "Feature request"
         for this functionality.  Please feel free to add to it with any 
         information that might help enable this functionality.
         
         
MAINTAINERS
-----------

Jon Freed, Facilitation Technologies LLC
